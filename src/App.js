import GameController from "./Game.js";
import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <GameController />
    </div>
  );
}
