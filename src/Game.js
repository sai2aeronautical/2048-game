import React, { useEffect, useState } from "react";

import {
  getEmptyBoard,
  randomNumber,
  swipeLeft,
  swipeRight,
  swipeUp,
  swipeDown,
  blocked,
  win
} from "./Box.js";

const Cell = ({ number }) => {
  return (
    <div className={`cell cell-${number}`}>{number > 0 ? number : ""}</div>
  );
};

const GameController = () => {
  const [board, setBoard] = useState(randomNumber(getEmptyBoard()));

  const checkEndGame = () => {
    if (win(board)) {
        window.alert("You win!");
    } else if (blocked(board)) {
      window.alert("Game over!");
    }
  };

  const left = () => {
    const newBoard = swipeLeft(board);
    setBoard(randomNumber(newBoard));
    checkEndGame();
  };

  const right = () => {
    const newBoard = swipeRight(board);
    setBoard(randomNumber(newBoard));
    checkEndGame();
  };

  const up = () => {
    const newBoard = swipeUp(board);
    setBoard(randomNumber(newBoard));
    checkEndGame();
  };

  const down = () => {
    const newBoard = swipeDown(board);
    setBoard(randomNumber(newBoard));
    checkEndGame();
  };

  const onKeyDown = (e) => {
    switch (e.key) {
      case "ArrowLeft":
        left();
        break;
      case "ArrowRight":
        right();
        break;
      case "ArrowUp":
        up();
        break;
      case "ArrowDown":
        down();
        break;

      default:
    }
  };

  useEffect(() => {
    window.addEventListener("keydown", onKeyDown);

    return () => {
      window.removeEventListener("keydown", onKeyDown);
    };
  });

  return (
    <>
      <div className="game-board">
        {board.map((row, i) => {
          return (
            <div key={`row-${i}`} className="row">
              {row.map((cell, j) => (
                <Cell key={`cell-${i}-${j}`} number={cell} />
              ))}
            </div>
          );
        })}
      </div>
    </>
  );
};

export default GameController;
