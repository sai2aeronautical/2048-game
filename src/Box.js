// Configuration for boxes and target score
let n =4;
let target = 2048;

export const getEmptyBoard = () => new Array(n).fill(new Array(n).fill(0)).map(a => a.slice())


export const hasValue = (board, value) =>{
    let hasValue = false;
    // eslint-disable-next-line
    board.map((eachRow,rowIndex) => {
        // eslint-disable-next-line
        eachRow.map((eachColumn, columnIndex) => {
            if(eachColumn === value){
                hasValue = true
            }
        })
    })
    return hasValue;
}

export const isFull = (board) => {
    console.log("Has Value ===>",hasValue(board,0))
    return !hasValue(board,0);
}

export const swipe = (board) => {
    const newBoard = getEmptyBoard()
    // eslint-disable-next-line
   board.map((eachRow,rowIndex)=>{
    let colIndex = 0;
    // eslint-disable-next-line
    eachRow.map((eachColumn,columnIndex)=>{
        if(eachColumn !== 0){
            newBoard[rowIndex][colIndex] = eachColumn;
            colIndex++
        }
    })
   })

    return newBoard
}

export const add  = (board) => {
   // eslint-disable-next-line
    board.map((eachRow,rowIndex)=>{
    // eslint-disable-next-line
     eachRow.map((eachColumn,columnIndex)=>{
         if(eachColumn !== 0 && eachColumn === eachRow[columnIndex+1] && columnIndex < n-1){
             board[rowIndex][columnIndex] = eachColumn *2;
             board[rowIndex][columnIndex+1] = 0
         }
     })
    })
 
     return board

}

export const swipeLeft = (board) => {
    const newBoard1 = swipe(board)
    const newBoard2 = add(newBoard1)

    const newBoard3 = swipe(newBoard2)

    return newBoard3;
}


export const rotateClockwise = (board) => {
    let newBoard = getEmptyBoard()
    // eslint-disable-next-line
    board.map((eachRow,rowIndex) => {
        // eslint-disable-next-line
        eachRow.map((eachColumn,columnIndex)=>{
            newBoard[rowIndex][columnIndex] = board[columnIndex][eachRow.length-rowIndex-1]
        })
    })

    return newBoard
}

export const rotateAntiClockwise = (board) => {
    let newBoard = getEmptyBoard()
    // eslint-disable-next-line
    board.map((eachRow,rowIndex) => {
        // eslint-disable-next-line
        eachRow.map((eachColumn,columnIndex)=>{
            newBoard[rowIndex][columnIndex] = board[eachRow.length-columnIndex-1][rowIndex]
        })
    })

    return newBoard;
}

export const rotateY = (board) => {
    let newBoard = getEmptyBoard()
    // eslint-disable-next-line
    board.map((eachRow,rowIndex) => {
        // eslint-disable-next-line
        eachRow.map((eachColumn,columnIndex)=>{
            newBoard[rowIndex][columnIndex] = board[rowIndex][eachRow.length-columnIndex-1]
        })
    })

    return newBoard;
}

export const swipeRight = (board) => {
    let rotateFirst = rotateY(board)
    
    let swipeLeftSide = swipeLeft(rotateFirst)

    let rotateAgain = rotateY(swipeLeftSide)

    return rotateAgain;
}

export const swipeUp = (board) => {
    let rotateClock = rotateClockwise(board)

    let swipeLeftSide = swipeLeft(rotateClock)

    let rotateAntiClock = rotateAntiClockwise(swipeLeftSide)

    return rotateAntiClock;
}

export const swipeDown = (board) => {

    let rotateAntiClock = rotateAntiClockwise(board)


    let swipeLeftSide = swipeLeft(rotateAntiClock)

    let rotateClock = rotateClockwise(swipeLeftSide)

    return rotateClock;
}


export const difference = (board,movedBoard) => {
    let isDiff = 0;
    // eslint-disable-next-line
    board.map((eachRow,rowIndex)=>{
        // eslint-disable-next-line
        eachRow.map((eachColumn,colIndex) => {
            if(board[rowIndex][colIndex] !== movedBoard[rowIndex][colIndex]){
                isDiff =1;
            }
        })
    })

    return isDiff
}

export const blocked = (board) => {
    let isBlocked = 1;
    if(difference(board,swipeLeft(board))){
        isBlocked = 0;
    }
    if(difference(board,swipeRight(board))){
        isBlocked = 0;
    }
    if(difference(board,swipeUp(board))){
        isBlocked = 0;
    }
    if(difference(board,swipeDown(board))){
        isBlocked = 0;
    }

    return isBlocked;
}

export const win = (board) => {
    let isWin = 0;
    if(hasValue(board,target)){
        isWin = 1;
    }
    
    return isWin;
}

export const gameOver = (board) => {
    let isOver = 0
    if(blocked(board)){
        isOver =1
    }

    return isOver
}

  
  export const randomNumber = (board) => {
      console.log("Initial====>",board)
    if (isFull(board)) {
      return board;
    }

    let rowIndex = Math.floor(Math.random()*n)
    let colIndex = Math.floor(Math.random()*n)

    while (board[rowIndex][colIndex] !== 0) {
         rowIndex = Math.floor(Math.random()*n)
         colIndex = Math.floor(Math.random()*n)
    }
  
    board[rowIndex][rowIndex] = Math.random() > 0.5 ? 4: 2;
    console.log("Final====>",board)
    return board;
  };